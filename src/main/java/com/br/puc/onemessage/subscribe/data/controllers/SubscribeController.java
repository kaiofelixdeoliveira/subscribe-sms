package com.br.puc.onemessage.subscribe.data.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.br.puc.onemessage.subscribe.data.requests.NewSmsRequest;
import com.br.puc.onemessage.subscribe.domain.usecases.MessageService;

@RestController
@CrossOrigin("*")
@RequestMapping("/subscribe")
public class SubscribeController {

	@Autowired
	private MessageService messageService;

	private static final Logger logger = LoggerFactory.getLogger(SubscribeController.class);


	@PostMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Void> saveMessageForApproval(@RequestBody NewSmsRequest message) {
		logger.info("save message");

		try {
			messageService.send(message);
			return new ResponseEntity<>(HttpStatus.OK);
		} catch (Exception e) {
			logger.info("Error {}", e);
			return new ResponseEntity<>(HttpStatus.BAD_GATEWAY);
		}

	}

}
