package com.br.puc.onemessage.subscribe.data.sends;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;

import com.br.puc.onemessage.subscribe.data.requests.QueueApprovalRequest;

@Service
public class SendMessage {

	private static final String DESTINATION_NAME = "approval-queue";
	private static final Logger logger = LoggerFactory.getLogger(SendMessage.class);

	@Autowired
	private JmsTemplate jmsTemplate;

	public void send(QueueApprovalRequest message) {
		logger.info("Sending message");
		jmsTemplate.convertAndSend(DESTINATION_NAME, message);
	}

}
