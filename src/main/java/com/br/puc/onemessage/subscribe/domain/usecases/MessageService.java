package com.br.puc.onemessage.subscribe.domain.usecases;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.br.puc.onemessage.subscribe.data.requests.NewSmsRequest;
import com.br.puc.onemessage.subscribe.data.requests.QueueApprovalRequest;
import com.br.puc.onemessage.subscribe.data.sends.SendMessage;

@Component
public class MessageService {

	private static final Logger logger = LoggerFactory.getLogger(MessageService.class);

	@Autowired
	private SendMessage sendMessage;

	public void send(NewSmsRequest message) {
		QueueApprovalRequest queueCorporateRequest = new QueueApprovalRequest();
		queueCorporateRequest.setMessage(message.getMessage());
		queueCorporateRequest.setPhone(message.getPhone());
		queueCorporateRequest.setCampaign(message.getCampaign());
		queueCorporateRequest.setDateTrigger(message.getDateTrigger());
		queueCorporateRequest.setDescription(message.getDescription());
		logger.info("[{}]", queueCorporateRequest.toString());
		sendMessage.send(queueCorporateRequest);
	}

}
