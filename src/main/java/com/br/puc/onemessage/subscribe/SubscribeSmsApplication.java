package com.br.puc.onemessage.subscribe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jms.annotation.EnableJms;

@EnableJms
@SpringBootApplication
public class SubscribeSmsApplication {

	public static void main(String[] args) {
		SpringApplication.run(SubscribeSmsApplication.class, args);
	}

}
